<?php get_header(); ?>

<article class="no-thumbnail">
	<div class="post-content">
		<div class="post-title">
			<h1>Page not found</h1>
		</div>
	</div>
    
    <div class="lineclear"></div>
</article>

<div class="lineclear"></div>

<?php get_footer(); ?>
