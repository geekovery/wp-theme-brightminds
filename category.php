<?php get_header(); ?>
<div class="tpl-category-php">
	<article class="<?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>" id="articles">
		<div class="header <?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>">
				<div class="share">
					<a class="twitter" href="https://twitter.com/home?status=<?php echo urlencode(home_url()); ?>" title="Partager sur Twitter" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/twitter.png" alt="Twitter Logo">
					</a>
					<a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(home_url()); ?>" title="Partager sur Facebook" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/facebook.png" alt="Facebook Logo">
					</a>
				</div>
		</div>

		<div class="post-content">
		<div class="post-title">
			<h1><?php single_cat_title(); ?></h1>
		</div>
			<section id="latest-posts">
				<?php if(!empty($recent_posts)): ?>
					<div class="listing-posts">
						<?php foreach ($recent_posts as $key => $recent_post) { ?>
							<div class="listing-posts-item">
								<?php
								$author = get_user_by('id', $recent_post->post_author);
								$categories_ids = wp_get_post_categories($recent_post->ID);
						    	$categories = [];
						    	foreach ($categories_ids as $key => $value) {
						    		$categorie = get_term($value);
						    		if(!empty($categorie)){
						    			$categories[] = $categorie;
						    		}
						    	} ?> 
						    	<div class="categories">
									<div class="categorie-item author"><span class="icon-author"><?php echo $author->user_nicename; ?></span> / <span class="icon-calendar"><?php echo date('d.m.Y', strtotime($recent_post->post_date)); ?></span></div>
									<?php if(!empty($categories)): ?>
										<?php foreach ($categories as $key => $categorie) { ?>
											<div class="categorie-item"><?php echo $categorie->name; ?></div>
										<?php } ?>
									<?php endif; ?>
									<div class="lineclear"></div>
								</div>
								<div class="image"><a href="<?php echo get_permalink($recent_post->ID); ?>" title="<?php echo $item->post_title; ?>"><?php echo get_the_post_thumbnail($recent_post->ID, 'post-main-image'); ?></a></div>
								<div class="post-title"><h3><a href="<?php echo get_permalink($recent_post->ID) ?>" title="<?php echo htmlspecialchars($recent_post->post_title); ?>"><?php echo $recent_post->post_title; ?></a></h3></div>
								<div class="inner-content">
									<?php $content = $recent_post->post_content;
									$content = apply_filters('the_content', $content);
									$content = str_replace(']]>', ']]&gt;', $content);
									echo wp_trim_words($content, 100); ?>
									<div class="read-more-container">
										<a href="<?php echo get_permalink($recent_post->ID); ?>" class="read-more"><?php echo __('Continue reading'); ?></a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<?php if($nb_pages > 1): ?>
						<div class="pager">
							<ul>
								<?php if($current_page > 0): ?>
									<li><a href="<?php echo home_url(); ?>/category/<?php echo $category_nicename; ?>?page=0#articles">&laquo;</a></li>
								<?php endif; ?>
								<?php $debut = ($current_page > 4) ? $current_page - 3 : 1;
								$max = ($current_page > 4) ? $current_page + 3 : $nb_pages;
								$max = ($max > $count_pages) ? $count_pages : $max; ?>
									<?php for ($i = $debut; $i <= $max; $i++) { ?>
										<li class="<?php echo ($i - 1 == $current_page) ? 'current' : ''; ?>"><a href="<?php echo home_url(); ?>/category/<?php echo $category_nicename; ?>?page=<?php echo $i - 1; ?>#articles"><?php echo $i; ?></a></li>
									<?php } ?>
								<?php if($current_page + 1 < $count_pages): ?>
									<li><a href="<?php echo home_url(); ?>/category/<?php echo $category_nicename; ?>?page=<?php echo $count_pages - 1; ?>#articles">&raquo;</a></li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<p>No posts were found.</p>
				<?php endif; ?>
			</section>
		</div>
	    
	    <div class="lineclear"></div>
	</article>
	<div class="lineclear"></div>
</div>

<?php get_footer(); ?>
