jQuery(document).ready(function(){
	jQuery(".c-hamburger").click(function(){
		jQuery(this).toggleClass('is-active');
		jQuery(".nav-container").toggleClass('is-active');
	});
	jQuery(".sidebar-toggle").click(function(){
		jQuery(this).toggleClass('active');
		jQuery("#homepage-wrapper .sidebar").toggleClass('active');
	});

	var top_button = jQuery(".top-of-page");
	var windowHeight = jQuery(window).height();
	jQuery(window).scroll(function(){
		var scrollTop = jQuery(window).scrollTop();
		if(scrollTop > windowHeight){
			if(!top_button.hasClass('scroll')){
				top_button.addClass('scroll');
			}
		}else{
			if(top_button.hasClass('scroll')){
				top_button.removeClass('scroll');
			}
		}
	});

	jQuery("#search-overlay .inner .close, #header-search").click(function(){
		jQuery("#search-overlay").fadeToggle(300);
		jQuery("#search-overlay .form-container input[name='search']").focus();
	});



	jQuery(window).load(function(){
		var sudoSlider = jQuery("#slider-archives").slidesjs(
			{
				pagination: {
			      active: false,
			      effect: "slide"
			    },
				navigation: {
			      active: true,
			      effect: "slide"
			    },
			    effect: {
			      slide: {
			        speed: 800
			      }
			    }
			}
		);
	});

});