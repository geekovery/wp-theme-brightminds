<?php

namespace Bundle\FrontBundle\Controller;

use Bundle\CoreBundle\Controller\Controller;
use Bundle\FrontBundle\Form\FormTest;

class IndexController extends Controller
{
    protected function get(){
        global $wpdb;
        $posts_per_page = 5;
        $current_month = false;
        $current_year = false;

        if(!empty($_GET['search'])){
            $count_posts = wp_count_posts();
            $published_posts = $count_posts->publish;
            $nb_pages = 1;
            $recent_posts = [];

            $search_results = [];
            $search = $_GET['search'];
            $search = strip_tags($search);
            $search = trim($search);
            $search = strtolower($search);

            $recent_posts = geekovery_get_posts_per_keyword($search);

        }elseif(!empty($_GET['archive'])){
            $archive = $_GET['archive'];
            $sql = "SELECT ID FROM {$wpdb->posts} WHERE post_type LIKE 'post' AND post_status LIKE 'publish' AND post_date LIKE '" . $_GET['archive'] . "-%'";
            $results = $wpdb->get_results($sql);
            if(!empty($results)){
                $recent_posts = [];
                foreach ($results as $key => $result) {
                    $recent_posts[] = get_post($result->ID);
                }
            }
            $current_month = $archive . '-01';
            $current_year = date('Y', strtotime($current_month));
            $current_month = date('F', strtotime($current_month));

        }else{
            $count_posts = wp_count_posts();
            $published_posts = $count_posts->publish;
            $nb_pages = ceil($published_posts / $posts_per_page);
            $args = array(
                'posts_per_page'   => $posts_per_page,
                'offset'           => (!empty($_GET['page']) ? ( ($_GET['page'] * $posts_per_page) ) : 0),
                'category'         => '',
                'category_name'    => '',
                'orderby'          => 'date',
                'order'            => 'DESC',
                'include'          => '',
                'exclude'          => '',
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => 'post',
                'post_mime_type'   => '',
                'post_parent'      => '',
                'author'           => '',
                'author_name'      => '',
                'post_status'      => 'publish',
                'suppress_filters' => true 
            );
            $recent_posts = get_posts( $args );
        }
        


        return array(
            'recent_posts' => $recent_posts,
            'count_posts' => $published_posts,
            'count_pages' => $nb_pages,
            'nb_pages' => ($nb_pages < 7) ? $nb_pages : 7,
            'current_page' => (!empty($_GET['page'])) ? $_GET['page'] : 0,
            'archives' => $archives,
            'current_month' => $current_month,
            'current_year' => $current_year
            
        );
    }

    protected function post(){
        return array();
    }
}
