<?php

namespace Bundle\FrontBundle\Controller;

use Bundle\CoreBundle\Controller\Controller;
use Bundle\FrontBundle\Form\FormTest;

class CategoryController extends Controller
{
    protected function get(){
        $category = get_category( get_query_var( 'cat' ) );
        $cat_id = $category->cat_ID;
        $posts_per_page = 5;
        $published_posts = $category->count;
        $nb_pages = ceil($published_posts / $posts_per_page);

        $args = array(
            'posts_per_page'   => $posts_per_page,
            'offset'           => (!empty($_GET['page']) ? ( ($_GET['page'] * $posts_per_page) ) : 0),
            'category'         => $cat_id,
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'post',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'           => '',
            'author_name'      => '',
            'post_status'      => 'publish',
            'suppress_filters' => true 
        );
        $recent_posts = get_posts( $args );

        


        return array(
            'recent_posts' => $recent_posts,
            'count_posts' => $published_posts,
            'count_pages' => $nb_pages,
            'nb_pages' => ($nb_pages < 7) ? $nb_pages : 7,
            'current_page' => (!empty($_GET['page'])) ? $_GET['page'] : 0,
            'category_nicename' => $category->category_nicename
        );
    }
}
