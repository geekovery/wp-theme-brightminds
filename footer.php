

    </div><!-- #page -->


<div class="lineclear"></div>

	<?php $footer_list_categories = get_categories( array(
	    'orderby' => 'name',
	    'order'   => 'ASC'
	) ); ?>
	<?php if(!empty($footer_list_categories)): ?>
		<?php if(sizeof($footer_list_categories) > 10){
			$footer_list_categories = array_slice($footer_list_categories, 0, 8);
		} ?>
		<div class="footer-list-categories">
			<ul>
			<?php foreach ( $footer_list_categories as $category ) {
			    printf( '<li><a href="%1$s">%2$s</a></li>',
			        esc_url( get_category_link( $category->term_id ) ),
			        esc_html( $category->name )
			    );
			} ?>
			</ul>
		</div>
	<?php endif; ?>

<?php if(false): ?>
<footer>
	<nav><?php wp_nav_menu("brightminds-footer"); ?></nav>
</footer>
<?php endif; ?>

<div id="search-overlay">
	<div class="inner">
		<div class="form-container">
			<div class="content mini">
				<form action="<?php echo home_url(); ?>" method="get">
					<input type="text" name="search" placeholder="<?php echo __('Search'); ?>..." value="<?php echo (!empty($_GET['search'])) ? $_GET['search'] : ''; ?>" />
					<input type="submit" value="<?php echo __('Search'); ?>" />
					<div class="lineclear"></div>
				</form>
			</div>
		</div>
		<div class="close">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/common/cancel.png" alt="Close">
		</div>
	</div>
</div>

<?php wp_footer(); ?>
</body>
</html>
