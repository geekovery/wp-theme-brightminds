<?php
/*
    Template Name: Contact
*/
get_header(); ?>

<article class="<?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>">
	<div class="header <?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>">
			<div class="share">
				<a class="twitter" href="https://twitter.com/home?status=<?php echo urlencode(get_permalink($post->ID)); ?>" title="Partager sur Twitter" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/twitter.png" alt="Twitter Logo">
				</a>
				<a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>" title="Partager sur Facebook" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/facebook.png" alt="Facebook Logo">
				</a>
			</div>
	</div>
	<?php if ( has_post_thumbnail() ): ?>
		<div class="large-image"><?php the_post_thumbnail( 'post-main-image' ); ?></div>
		<?php if(!empty($thumbnail) && (!empty($thumbnail->post_title) || !empty($thumbnail->post_excerpt)) ) ?>
		<div class="image-legend">
			<?php if(!empty($thumbnail->post_title)){ ?><span class="image-legend-title"><?php echo $thumbnail->post_title; ?></span><?php } ?>
			<?php if(!empty($thumbnail->post_excerpt)){ ?><span class="image-legend-content"><?php echo nl2br($thumbnail->post_excerpt); ?></span><?php } ?>
		</div>
	<?php endif; ?>

	<div class="post-content">
		<div class="post-title">
			<h1><?php echo $post->post_title; ?></h1>
		</div>
		<div class="inner-content">
			<?php $content = $post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content; ?>
		</div>

		<div class="lineclear"></div>

	    <div class="form-container">
	    	
	        <?php if($form_valid): ?>
	            <p class="valid"><?php echo $form_message; ?></p>
	        <?php else: ?>
			<form name="contact" class="contact" method="post" action="">
				<input type="email" name="email" placeholder="Email" value="<?php echo (!empty($_POST['email'])) ? $_POST['email'] : ''; ?>" />
				<textarea name="message" placeholder="Your message here..." ><?php echo (!empty($_POST['message'])) ? $_POST['message'] : ''; ?></textarea>
	            <?php $newToken = generateFormToken('form_contact');    ?>
	            <input type="hidden" name="token" value="<?php echo $newToken; ?>">
	            <?php if(!empty($form_errors)): ?>
	                <div class="errors">
	                    <?php foreach ($form_errors as $key => $error) {
	                        echo "<p>" . $error . "</p>";
	                    } ?>
	                </div>
	            <?php endif; ?>
				<input type="submit" name="submit" value="Send" />
			</form>
	        <?php endif; ?>
	    </div>
	</div>
    
    <div class="lineclear"></div>

</article>

<div class="lineclear"></div>
<?php get_footer(); ?>
