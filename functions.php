<?php
require_once(get_stylesheet_directory() . '/vendor/autoload.php');
require_once(get_stylesheet_directory() . '/src/Bootstrap.php');


define('TEMPLATE_DIR', get_bloginfo('template_directory'));
define('TEMPLATE_URL', get_bloginfo('template_url'));

session_start();
/* custom images sizes */

add_image_size( 'post-main-image', '1200', '420', array( 'center', 'center' ) );
add_image_size( 'post-main-image-tablet', '768', '380', array( 'center', 'center' ) );
add_image_size( 'post-main-image-mobile', '320', '220', array( 'center', 'center' ) );
add_image_size( 'header-main-image', '2000', '1200', array( 'center', 'center' ) );
add_image_size( 'post-image-list-2', '560', '320', array( 'center', 'center' ) );


/* CUSTOMIZER */

if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}
require get_parent_theme_file_path( '/inc/custom-header.php' );
require get_parent_theme_file_path( '/inc/template-tags.php' );
require get_parent_theme_file_path( '/inc/template-functions.php' );
require get_parent_theme_file_path( '/inc/customizer.php' );
require get_parent_theme_file_path( '/inc/icon-functions.php' );


/*

TO DO : -------------

# Footer - Add bouton remontée
# Category page

*/


function geekovery_get_posts_per_keyword($search){
	global $wpdb;
	$found = [];
	$query_post_name = "SELECT ID FROM {$wpdb->posts} WHERE post_title LIKE '%" . $search . "%'";
    $query_post_name_results = $wpdb->get_results($query_post_name);
    if(!empty($query_post_name_results)){
        foreach ($query_post_name_results as $key => $query_post_name_result) {
            if(!isset($search_results[$query_post_name_result->ID])){
                $search_results[$query_post_name_result->ID] = 0;
            }
            $count = $search_results[$query_post_name_result->ID];
            $search_results[$query_post_name_result->ID] = $count+5;
        }
    }
    $query_post_name = "SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%" . $search . "%'";
    $query_post_name_results = $wpdb->get_results($query_post_name);
    if(!empty($query_post_name_results)){
        foreach ($query_post_name_results as $key => $query_post_name_result) {
            if(!isset($search_results[$query_post_name_result->ID])){
                $search_results[$query_post_name_result->ID] = 0;
            }
            $count = $search_results[$query_post_name_result->ID];
            $search_results[$query_post_name_result->ID] = $count+2;
        }
    }
    $query_post_name = "SELECT tr.object_id
    FROM {$wpdb->terms} tm, {$wpdb->term_relationships} tr, {$wpdb->term_taxonomy} tt
    WHERE tr.term_taxonomy_id = tt.term_taxonomy_id
    AND tt.term_id = tm.term_id
    AND tm.name LIKE '%" . $search . "%'";
    $query_post_name_results = $wpdb->get_results($query_post_name);
    if(!empty($query_post_name_results)){
        foreach ($query_post_name_results as $key => $query_post_name_result) {
            if(!isset($search_results[$query_post_name_result->ID])){
                $search_results[$query_post_name_result->ID] = 0;
            }
            $count = $search_results[$query_post_name_result->ID];
            $search_results[$query_post_name_result->ID] = $count+1;
        }
    }
    if(!empty($search_results)){
        foreach ($search_results as $id => $sort) {
            $found[] = get_post($id);
        }
    }
	return $found;
}

function generateFormToken($form) {
    
    // generate a token from an unique value
    $token = md5(uniqid(microtime(), true));  

    // Write the generated token to the session variable to check it against the hidden field when the form is sent
    $_SESSION[$form.'_token'] = $token; 

    return $token;

}

function verifyFormToken($form) {
    
    // check if a session is started and a token is transmitted, if not return an error
    if(!isset($_SESSION[$form.'_token'])) { 
        return false;
    }
    
    // check if the form is sent with token in it
    if(!isset($_POST['token'])) {
        return false;
    }
    
    // compare the tokens against each other if they are still the same
    if ($_SESSION[$form.'_token'] !== $_POST['token']) {
        return false;
    }
    
    return true;
}



function test_input($data) {
  $data = strip_tags($data);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}