<?php

namespace Bundle\FrontBundle\Controller;

use Bundle\CoreBundle\Controller\Controller;

class SingleController extends Controller
{
    protected function get(){
    	global $post;

    	$categories_ids = wp_get_post_categories($post->ID);
    	$categories = [];
    	foreach ($categories_ids as $key => $value) {
    		$categorie = get_term($value);
    		if(!empty($categorie)){
    			$categories[] = $categorie;
    		}
    	}

    	$author = get_user_by('id', $post->post_author);

    	$thumbnail = get_post(get_post_thumbnail_id());

    	$related = get_posts(
    		array(
    			'category__in' => wp_get_post_categories($post->ID),
    			'numberposts' => 2,
    			'post__not_in' => array($post->ID),
    			'posts_per_page' => 2,
    		)
    	);


        return array(
        	'categories' => $categories,
        	'author' => $author,
        	'thumbnail' => $thumbnail,
        	'related' => $related
        );
    }
}
