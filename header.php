<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title('&raquo;', true, 'left'); ?></title>
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Roboto+Slab:300,400,700" rel="stylesheet">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>  id="top">
	<header>
		<div class="content">
		<div class="website-infos">
		<?php the_custom_logo(); ?>
		<?php if(function_exists('brightminds_the_custom_logo')){ brightminds_the_custom_logo(); } ?>
		<div class="website-title"><a href="<?php echo home_url(); ?>" title="Homepage"><?php bloginfo( 'name' ); ?></a></div>
		<?php $description = get_bloginfo( 'description', 'display' );
		if ( $description || is_customize_preview() ) : ?>
			<p class="site-description"><?php echo $description; ?></p>
		<?php endif; ?>
		</div>
			
		<div class="nav-container">
			<nav>
				<?php wp_nav_menu("brightminds-header"); ?>
				<div id="header-search">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/common/search-grey.png" alt="Rechercher">
				</div>
			</nav>
			<div class="lineclear"></div>
			<button class="c-hamburger c-hamburger--htx">
		      <span>Menu</span>
		    </button>
		</div>
		<div class="lineclear"></div>
		</div>
	</header>

	<?php if ( is_front_page() ) : ?>
		<div class="custom-header-media">
			<div class="content content-max"><?php the_custom_header_markup(); ?></div>
		</div>
	<?php endif; ?>

	<div class="lineclear"></div>
	
    <div id="page">

