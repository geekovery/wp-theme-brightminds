<?php

namespace Bundle\FrontBundle\Controller;

use Bundle\CoreBundle\Controller\Controller;

class TemplateContactController extends Controller
{
    protected function get(){
    	global $post;

    	$thumbnail = get_post(get_post_thumbnail_id());

        return array(
        	'thumbnail' => $thumbnail
        );
    }



    protected function post(){
        global $post;
        global $wpdb;

        $errors = [];
        $error = false;
        $message = "";
        $valid = false;

        if(!empty($_POST)){
            $valid = false;

            if (verifyFormToken('form_contact')) {

                $ip = $_SERVER["REMOTE_ADDR"];
                $email = test_input($_POST['email']);
                $message = test_input($_POST['message']);

                

                if(!isset($message) || empty($message)){
                    $errors['message'] = "The message must be filled.";
                }

                if(!empty($email)){
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

                        $to = get_option('admin_email', 'geminhya@gmail.com');
                        $subject = bloginfo( 'name' ) . ' - Contact Form';
                        $body = "<strong>FROM : </strong>" . $email . "<br><br><strong>MESSAGE : </strong>" . $message. "<br><br><a href='" . home_url() . "' target='_blank'>" . bloginfo( 'name' ) . "</a>";
                        $headers = array('Content-Type: text/html; charset=UTF-8');
                        $headers[] = 'From: ' . bloginfo( 'name' ) . ' <' . get_option('admin_email') . '>';
                         
                        wp_mail( $to, $subject, $body, $headers );

                        // Create post object
                        $my_post = array(
                          'post_title'    => date('Y-m-d') . " Contact Form Message",
                          'post_content'  => "<p>" . $body . "<p>",
                          'post_status'   => 'draft',
                          'post_author'   => 1,
                        );
                         
                        // Insert the post into the database
                        $post_id = wp_insert_post( $my_post );
                        if(!empty($post_id)){
                            update_post_meta($post_id, 'ip_adress', $ip);
                            update_post_meta($post_id, 'email', $email);
                            update_post_meta($post_id, 'message', $message);
                            update_post_meta($post_id, 'sent_date', date('Y-m-d H:i'));
                        }

                        $valid = true;
                        $message = "Thank you, your message has been sent.";


                    } else {
                      $errors['email'] = "The email is not valid.";
                    }
                }else{
                    $errors['email'] = "The email must be filled.";
                }

            } else {
               
               $message = "Hack-Attempt detected. Got ya!.";
               $errors['hack'] = $message;
               $error = true;
            }
        }


    	$thumbnail = get_post(get_post_thumbnail_id());
        return array(
            'form_errors' => $errors,
            'form_valid' => $valid,
            'form_message' => $message,
        	'thumbnail' => $thumbnail
        );
       }
      
}
