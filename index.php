<?php get_header(); ?>
<h1 class="hidden"><?php bloginfo( 'name' ); ?></h1>
<div id="homepage-wrapper">
	<article class="<?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>" id="articles">
		<div class="header <?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>">
				<div class="share">
					<a class="twitter" href="https://twitter.com/home?status=<?php echo urlencode(home_url()); ?>" title="Partager sur Twitter" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/twitter.png" alt="Twitter Logo">
					</a>
					<a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(home_url()); ?>" title="Partager sur Facebook" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/facebook.png" alt="Facebook Logo">
					</a>
				</div>
		</div>

		<div class="post-content">
			<section id="latest-posts">
				<h2>Articles<?php if(!empty($_GET['search'])){ ?> <span><i><?php echo _e('for') ?></i> &laquo;&nbsp;<?php echo $_GET['search']; ?>&nbsp;&raquo;</span><?php } ?><?php if(!empty($current_month) && !empty($current_year)){ ?> <span><i><?php echo _e('in'); ?></i> &laquo;&nbsp;<?php echo _e($current_month) . ' ' . $current_year; ?>&nbsp;&raquo;</span><?php } ?></h2>
				<?php if(!empty($recent_posts)): ?>
					<div class="listing-posts">
						<?php foreach ($recent_posts as $key => $recent_post) { ?>
							<div class="listing-posts-item">
								<?php
								$author = get_user_by('id', $recent_post->post_author);
								$categories_ids = wp_get_post_categories($recent_post->ID);
						    	$categories = [];
						    	foreach ($categories_ids as $key => $value) {
						    		$categorie = get_term($value);
						    		if(!empty($categorie)){
						    			$categories[] = $categorie;
						    		}
						    	} ?> 
						    	<div class="categories">
									<div class="categorie-item author"><span class="icon-author"><?php echo $author->user_nicename; ?></span> / <span class="icon-calendar"><?php echo date('d.m.Y', strtotime($recent_post->post_date)); ?></span></div>
									<?php if(!empty($categories)): ?>
										<?php foreach ($categories as $key => $categorie) { ?>
											<div class="categorie-item"><?php echo $categorie->name; ?></div>
										<?php } ?>
									<?php endif; ?>
									<div class="lineclear"></div>
								</div>
								<div class="image"><a href="<?php echo get_permalink($recent_post->ID); ?>" title="<?php echo $item->post_title; ?>"><?php echo get_the_post_thumbnail($recent_post->ID, 'post-main-image-tablet'); ?></a></div>
								<div class="post-title"><h3><a href="<?php echo get_permalink($recent_post->ID) ?>" title="<?php echo htmlspecialchars($recent_post->post_title); ?>"><?php echo $recent_post->post_title; ?></a></h3></div>
								<div class="inner-content">
									<?php $content = $recent_post->post_content;
									$content = apply_filters('the_content', $content);
									$content = str_replace(']]>', ']]&gt;', $content);
									echo wp_trim_words($content, 100); ?>
									<div class="read-more-container">
										<a href="<?php echo get_permalink($recent_post->ID); ?>" class="read-more"><?php echo __('Continue reading'); ?></a>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<?php if($nb_pages > 1): ?>
						<div class="pager">
							<ul>
								<?php if($current_page > 0): ?>
									<li><a href="<?php echo home_url(); ?>?page=0#articles">&laquo;</a></li>
								<?php endif; ?>
								<?php $debut = ($current_page > 4) ? $current_page - 3 : 1;
								$max = ($current_page > 4) ? $current_page + 3 : $nb_pages;
								$max = ($max > $count_pages) ? $count_pages : $max; ?>
									<?php for ($i = $debut; $i <= $max; $i++) { ?>
										<li class="<?php echo ($i - 1 == $current_page) ? 'current' : ''; ?>"><a href="<?php echo home_url(); ?>?page=<?php echo $i - 1; ?>#articles"><?php echo $i; ?></a></li>
									<?php } ?>
								<?php if($current_page + 1 < $count_pages): ?>
									<li><a href="<?php echo home_url(); ?>?page=<?php echo $count_pages - 1; ?>#articles">&raquo;</a></li>
								<?php endif; ?>
							</ul>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<p>No posts were found.</p>
				<?php endif; ?>
			</section>
		</div>

		<div class="sidebar">
			<div class="sidebar-content">
				<section class="search">
				<form action="" method="get">
					<input type="text" name="search" placeholder="<?php echo __('Search'); ?>" value="<?php echo (!empty($_GET['search'])) ? $_GET['search'] : ''; ?>" />
					<input type="submit" value="Search" />
					<div class="lineclear"></div>
				</form>
				
			</section>
			<section class="categories">
				<h2><?php echo _e('Categories'); ?></h2>
				<?php $categories = get_categories( array(
				    'orderby' => 'name',
				    'order'   => 'ASC'
				) ); ?>
				<ul>
				<?php foreach ( $categories as $category ) {
				    printf( '<li><a href="%1$s">%2$s <span>(%3$s)</span></a></li>',
				        esc_url( get_category_link( $category->term_id ) ),
				        esc_html( $category->name ),
				        esc_html( $category->count )
				    );
				} ?>
				</ul>
				
			</section>
			<?php $comments = get_comments(array('number' => 10)); ?>
			<?php if(!empty($comments)): ?>
				<section class="recent-comments">
					<h2><?php echo _e('Recent Comments'); ?></h2>
					<ul class="list-links">
					<?php foreach ($comments as $key => $comment) { $com_post = get_post($comment->comment_post_ID); ?>
						<li><?php echo $comment->comment_author; ?> on <a href="<?php echo get_permalink($com_post->ID); ?>#post-comment-<?php echo $comment->comment_ID; ?>"><?php echo $com_post->post_title; ?></a></li>
					<?php } ?>
					</ul>
				</section>
			<?php endif; ?>
			<?php $archives = [];
			$sql = "SELECT post_date FROM {$wpdb->posts} WHERE post_type LIKE 'post' AND post_status LIKE 'publish' ORDER BY post_date DESC";
			$results = $wpdb->get_results($sql);
			if(!empty($results) && is_array($results)){
				foreach ($results as $key => $entity) {
					$year = date('Y', strtotime($entity->post_date));
					$year_month = date('Y-m', strtotime($entity->post_date));
					if(!isset($archives[$year])){
						$archives[$year] = [];
					}
					if(!isset($archives[$year][$year_month])){
						$archives[$year][$year_month] = date('F', strtotime($entity->post_date));
					}
				}
				if(!empty($archives)){
				 ?>
					<section class="archives">
						<h2><?php echo _e('Archives'); ?></h2>
						<div id="slider-archives">
							<?php foreach ($archives as $year => $archive) { $archive = array_reverse($archive); ?>
							<div class="slide">
								<div class="year"><?php echo $year; ?></div>
								<div class="months">
									<?php for ($i=1; $i <= 12; $i++) {
									$formkey = $year . '-' . ( $i < 10 ? '0' . $i : $i ); ?>
										<?php if(!empty($archive[$formkey])): ?>
											<a href="<?php echo home_url(); ?>?archive=<?php echo $formkey; ?>"><?php echo  __($archive[$formkey]); ?></a>
										<?php else: ?>
											<a href="#" class="empty"><?php echo ($i < 10 ? '0' . $i : $i); ?></a>
										<?php endif; ?>
									<?php } ?>
									<div class="lineclear"></div>
								</div>
							</div>
							<?php } ?>
						</div>
					</section>
				<?php }
			}

			?>
			</div>
			<div class="lineclear"></div>
		</div>
	    
	    <div class="lineclear"></div>
	</article>

	<div class="lineclear"></div>
	<div class="sidebar-toggle"></div>
	<a href="#top" class="top-of-page">Top</a>
</div>

<?php get_footer(); ?>
