<?php

namespace Bundle\FrontBundle\Controller;

use Bundle\CoreBundle\Controller\Controller;

class PageController extends Controller
{
    protected function get(){
    	global $post;

    	$thumbnail = get_post(get_post_thumbnail_id());

        return array(
        	'thumbnail' => $thumbnail
        );
    }
}
