<?php get_header(); ?>

<article class="<?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>">
	<div class="header <?php echo (has_post_thumbnail()) ? 'has-thumbnail' : 'no-thumbnail'; ?>">
		<div class="categories">
		<div class="categorie-item author"><?php echo $author->user_nicename; ?> / <?php echo date('d.m.Y', strtotime($post->post_date)); ?></div>
			<?php if(!empty($categories)): ?>
				<?php foreach ($categories as $key => $categorie) { ?>
					<div class="categorie-item"><?php echo $categorie->name; ?></div>
				<?php } ?>
			<?php endif; ?>
		</div>
			<div class="share">
				<a class="twitter" href="https://twitter.com/home?status=<?php echo urlencode(get_permalink($post->ID)); ?>" title="Partager sur Twitter" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/twitter.png" alt="Twitter Logo">
				</a>
				<a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>" title="Partager sur Facebook" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/front/images/icons/social/facebook.png" alt="Facebook Logo">
				</a>
			</div>
	</div>
	<?php if ( has_post_thumbnail() ): ?>
		<div class="large-image"><?php the_post_thumbnail( 'post-main-image' ); ?></div>
		<?php if(!empty($thumbnail) && !empty($thumbnail->post_excerpt) ): ?>
			<div class="image-legend">
				<?php if(!empty($thumbnail->post_excerpt)){ ?><span class="image-legend-content"><?php echo nl2br($thumbnail->post_excerpt); ?></span><?php } ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>

	<div class="post-content">
		<div class="post-title">
			<h1><?php echo $post->post_title; ?></h1>
		</div>
		<div class="inner-content">
			<?php $content = $post->post_content;
			$content = apply_filters('the_content', $content);
			$content = str_replace(']]>', ']]&gt;', $content);
			echo $content; ?>
		</div>
		<div class="tags">
			<?php $tags = wp_get_post_tags($post->ID);
			if(!empty($tags)){
				echo "<strong>Tags : </strong>";
				foreach ($tags as $key => $tag) {
					echo $tag->name;
					if($key < sizeof($tags) - 1){
						echo ", ";
					}
				}
			} ?>
		</div>
	</div>
    
    <div class="navigation-posts">
	    <div class="navigation-item previous-post"><?php previous_post_link('%link', '%title'); ?></div>
	    <div class="navigation-item next-post"><?php next_post_link('%link', '%title'); ?></div>
    </div>
    <div class="lineclear"></div>

    <?php if(!empty($related)): ?>
    	<div class="part-section related-posts">
    		<h2>Related Posts</h2>
    		<div class="list-posts">
    			<?php $count_posts = 1; foreach ($related as $key => $item) { ?>
    				<div class="list-posts-item <?php echo ($count_posts % 2 == 0) ? 'last' : ''; ?>">
    					<div class="image"><a href="<?php echo get_permalink($item->ID); ?>" title="<?php echo $item->post_title; ?>"><?php echo get_the_post_thumbnail($item->ID, 'post-image-list-2'); ?></a></div>
    					<div class="title">
    						<a href="<?php echo get_permalink($item->ID); ?>" title="<?php echo $item->post_title; ?>"><?php echo $item->post_title; ?></a>
    					</div>
    				</div>
    				<?php if($count_posts % 2 == 0){ ?><div class="lineclear"></div><?php } ?>
    			<?php $count_posts++; } ?>
    			<div class="lineclear"></div>
    		</div>
    		<div class="lineclear"></div>
    	</div>
    <?php endif; ?>

    <div class="lineclear"></div>

	<div class="part-section comment-section">
		<h2>Comments</h2>
		<div class="content">
			<?php $comments_args = array(
		        // Change the title of send button 
		        'label_submit' => __( 'Send', 'textdomain' ),
		        // Change the title of the reply section
		        'title_reply' => __( 'Write a Comment', 'textdomain' ),
		        // Remove "Text or HTML to be displayed after the set of comment fields".
		        'comment_notes_after' => '',
		        // Redefine your own textarea (the comment body).
		        'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><br /><textarea id="comment" name="comment" aria-required="true"></textarea></p>',
			);
			comment_form( $comments_args ); ?>
		</div>
		<?php $comments = get_comments(array('post_id' => $post->ID, 'number' => 10)); ?>
		<?php if(!empty($comments)): ?>
			<div class="list-comments">
				<?php foreach ($comments as $key => $comment) { ?>
					<div class="comment" id="post-comment-<?php echo $comment->comment_ID; ?>">
						<div class="comment-infos">
						<h3><?php echo (!empty($comment->comment_author_url)) ? "<a href='" . $comment->comment_author_url . "' target='_blank'>" . $comment->comment_author . "</a>" : $comment->comment_author ?></h3>
						<span><?php echo $comment->comment_date; ?></span>
						</div>
						<p><?php echo nl2br($comment->comment_content); ?></p>
					</div>
				<?php } ?>
			</div>
		<?php endif; ?>
	</div>

	<div class="lineclear"></div>

</article>



<div class="lineclear"></div>

<?php get_footer(); ?>
